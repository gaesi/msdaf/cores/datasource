# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

start_time = Time.now
puts "Opening CSV at #{start_time}".colorize(:yellow)
file_name = 'extrator-2018-5.csv'
file_name = 'farmpop2018-sample.csv' if Rails.env.test?
dataset_path = File.join(Rails.root, 'modelling', 'extracted-datasets', 'farmpop', file_name)

count = 0
headers = []
dispensations = []
File.open(dataset_path, 'r:ISO-8859-1') do |f|
  while line = f.gets
    dispensation = []
    line = line.strip
    columns = line.split(';')
    if count == 0
      headers = columns
      count = count+1
      next
    end

    columns.each_with_index do |val, i|
      attr = headers[i]
      val = '-1' if val.nil?
      val = val.tr('"', '') if val.is_a?(String) && !val.index('"').nil?

      if !attr.nil? && (!attr.index('VL').nil? || !attr.index('QT').nil?)
        if val.is_a?(String) && !val.index(',').nil?
          val.gsub! ',', '.'
          val = '0' + val if val[0] == '.'
        end

        val = BigDecimal(val)
      end

      if !attr.index('DT_').nil? && !val.nil?
        if attr == 'DT_NASCIMENTO'
          headers.each_with_index do |attr, hi|
            if attr == 'CO_ANO_NASCIMENTO'
              columns[hi].nil? ? n_val = val[0..5] + 19 + val[-2..-1] : n_val = val[0..5] + columns[hi]
              val = n_val
            end
          end
        end

        val = Date.parse val rescue p 'val DT invalida: ' + val
      end

      if ['NU_ALTITUDE_MUN_PAC', 'NU_LONGITUDE_MUN_PAC', 'NU_ALTITUDE_MUN_EST'].include? attr
        val = val.to_f
      end

      if ['CO_AGLOMERADO_URBANO_PAC', 'CO_MICRORREGIAO_PAC'].include? attr
        val = val.to_i
      end

      dispensation << val
    end

    dispensations << dispensation
    count = count+1

    if count % 150000 == 0
      p "#{count} iterated registers at #{Time.now}"
      start_time = Time.now
      puts 'Insertion started'.colorize(:yellow)
      Dispensation.import headers, dispensations, validate: false
      end_time = Time.now
      p "Spent Time (insertion): #{end_time - start_time}"

      # count == 1000000 ? break : count=count+1 # TODO Comment if not a test
      dispensations = [] #free memory
      # break
    end
  end
end