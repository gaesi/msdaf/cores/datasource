# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_07_113712) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dispensations", force: :cascade do |t|
    t.datetime "DT_DISPENSACAO", null: false
    t.integer "CO_SEQ_DISPENSACAO", null: false
    t.integer "QT_DISPENSACAO", null: false
    t.integer "NU_COMPETENCIA_DISPENSACAO", null: false
    t.decimal "VL_UNITARIO", null: false
    t.decimal "VL_REFERENCIA_POPFARMA", null: false
    t.integer "CO_SEQ_ESTABELECIMENTO", null: false
    t.string "NO_CID", null: false
    t.integer "CO_CID", null: false
    t.integer "ST_REGISTRO_ATIVO_CID", null: false
    t.boolean "ST_VIVO", null: false
    t.integer "CO_PESSOA", null: false
    t.date "DT_NASCIMENTO", null: false
    t.integer "DS_MES_NASCIMENTO", null: false
    t.integer "CO_ANO_NASCIMENTO", null: false
    t.string "SG_SEXO_PACIENTE", null: false
    t.integer "CO_MUNICIPIO_IBGE_PAC", null: false
    t.string "NO_MUNICIPIO_PAC", null: false
    t.integer "CO_AGLOMERADO_URBANO_PAC", null: false
    t.string "NO_AGLOMERADO_URBANO_PAC", null: false
    t.integer "CO_MACRORREGIONAL_SAUDE_PAC", null: false
    t.string "NO_MACRORREGIONAL_SAUDE_PAC", null: false
    t.integer "CO_MESORREGIAO_PAC", null: false
    t.string "NO_MESORREGIAO_PAC", null: false
    t.integer "CO_MICRORREGIAO_PAC", null: false
    t.string "NO_MICRORREGIAO_PAC", null: false
    t.integer "CO_MICRORREGIONAL_SAUDE_PAC", null: false
    t.string "NO_MICRORREGIONAL_SAUDE_PAC", null: false
    t.string "SG_UF_PAC", null: false
    t.string "NO_UF_PAC", null: false
    t.float "NU_ALTITUDE_MUN_PAC", null: false
    t.float "NU_UF_LONGITUDE_PAC", null: false
    t.float "NU_LONGITUDE_MUN_PAC", null: false
    t.string "NO_REGIAO_SAUDE_PAC", null: false
    t.boolean "ST_PARTICIPA_POPFARMA_PAC", null: false
    t.boolean "ST_PART_FARMACIA_POPULAR_PAC", null: false
    t.string "NO_PRODUTO", null: false
    t.integer "CO_PRODUTO", null: false
    t.integer "NU_CATMAT", null: false
    t.integer "CO_PATOLOGIA", null: false
    t.string "DS_PATOLOGIA", null: false
    t.string "DS_APRESENTACAO", null: false
    t.string "DS_UNID_APRESENTACAO", null: false
    t.string "SG_UNID_APRESENTACAO", null: false
    t.string "DS_UNID_CONCENTRACAO", null: false
    t.string "SG_UNID_CONCENTRACAO", null: false
    t.float "QT_MAXIMA", null: false
    t.float "QT_USUAL", null: false
    t.integer "CO_PRINCIPIO_ATIVO_MEDICAMENTO", null: false
    t.string "NO_FABRICANTE", null: false
    t.string "NU_REGISTRO_ANVISA", null: false
    t.integer "CO_GRUPO_FINANCIAMENTO", null: false
    t.string "DS_GRUPO_FINANCIAMENTO", null: false
    t.decimal "VL_PRECO_SUBSIDIADO", null: false
    t.float "QT_PRESCRITA", null: false
    t.float "QT_SOLICITADA", null: false
    t.float "QT_ESTORNADA", null: false
    t.integer "NU_LINHA_CUIDADO", null: false
    t.string "DS_PROGRAMA_SAUDE", null: false
    t.string "SG_PROGRAMA_SAUDE", null: false
    t.string "TP_PROGRAMA_SAUDE", null: false
    t.boolean "ST_PARTICIPA_POPFARMA_EST", null: false
    t.boolean "ST_PART_FARMACIA_POPULAR_EST", null: false
    t.integer "QT_POPULACA_PORTARIA_1555_2013", null: false
    t.integer "CO_AGLOMERADO_URBANO_EST", null: false
    t.string "NO_AGLOMERADO_URBANO_EST", null: false
    t.integer "CO_MACRORREGIONAL_SAUDE_EST", null: false
    t.string "NO_MACRORREGIONAL_SAUDE_EST", null: false
    t.integer "CO_MESORREGIAO_EST", null: false
    t.string "NO_MESORREGIAO_EST", null: false
    t.integer "CO_MICRORREGIAO_EST", null: false
    t.string "NO_MICRORREGIAO_EST", null: false
    t.integer "CO_MICRORREGIONAL_SAUDE_EST", null: false
    t.string "NO_MICRORREGIONAL_SAUDE_EST", null: false
    t.string "SG_UF_EST", null: false
    t.string "NO_UF_EST", null: false
    t.float "NU_ALTITUDE_MUN_EST", null: false
    t.float "NU_UF_LONGITUDE_EST", null: false
    t.float "NU_LONGITUDE_MUN_EST", null: false
    t.string "NO_REGIAO_SAUDE_EST", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
