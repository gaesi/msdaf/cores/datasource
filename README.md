# "Dispensation" Model/API
Code to generate the CRUD Model:
```shell
  $ rails g scaffold Dispensation DT_DISPENSACAO:datetime CO_SEQ_DISPENSACAO:integer QT_DISPENSACAO:integer NU_COMPETENCIA_DISPENSACAO:integer VL_UNITARIO:decimal VL_REFERENCIA_POPFARMA:decimal CO_SEQ_ESTABELECIMENTO:integer NO_CID:string CO_CID:integer ST_REGISTRO_ATIVO_CID:integer ST_VIVO:boolean CO_PESSOA:integer DT_NASCIMENTO:date DS_MES_NASCIMENTO:integer CO_ANO_NASCIMENTO:integer SG_SEXO_PACIENTE:string CO_MUNICIPIO_IBGE_PAC:integer NO_MUNICIPIO_PAC:string CO_AGLOMERADO_URBANO_PAC:integer NO_AGLOMERADO_URBANO_PAC:string CO_MACRORREGIONAL_SAUDE_PAC:integer NO_MACRORREGIONAL_SAUDE_PAC:string CO_MESORREGIAO_PAC:integer NO_MESORREGIAO_PAC:string CO_MICRORREGIAO_PAC:integer NO_MICRORREGIAO_PAC:string CO_MICRORREGIONAL_SAUDE_PAC:integer NO_MICRORREGIONAL_SAUDE_PAC:string SG_UF_PAC:string NO_UF_PAC:string NU_ALTITUDE_MUN_PAC:float NU_UF_LONGITUDE_PAC:float NU_LONGITUDE_MUN_PAC:float NO_REGIAO_SAUDE_PAC:string ST_PARTICIPA_POPFARMA_PAC:boolean ST_PART_FARMACIA_POPULAR_PAC:boolean NO_PRODUTO:string CO_PRODUTO:integer NU_CATMAT:integer CO_PATOLOGIA:integer DS_PATOLOGIA:string DS_APRESENTACAO:string DS_UNID_APRESENTACAO:string SG_UNID_APRESENTACAO:string DS_UNID_CONCENTRACAO:string SG_UNID_CONCENTRACAO:string QT_MAXIMA:float QT_USUAL:float CO_PRINCIPIO_ATIVO_MEDICAMENTO:integer NO_FABRICANTE:string NU_REGISTRO_ANVISA:integer CO_GRUPO_FINANCIAMENTO:integer DS_GRUPO_FINANCIAMENTO:string VL_PRECO_SUBSIDIADO:decimal QT_PRESCRITA:float QT_SOLICITADA:float QT_ESTORNADA:float NU_LINHA_CUIDADO:integer DS_PROGRAMA_SAUDE:string SG_PROGRAMA_SAUDE:string TP_PROGRAMA_SAUDE:string ST_PARTICIPA_POPFARMA_EST:boolean ST_PART_FARMACIA_POPULAR_EST:boolean QT_POPULACA_PORTARIA_1555_2013:integer CO_AGLOMERADO_URBANO_EST:integer NO_AGLOMERADO_URBANO_EST:string CO_MACRORREGIONAL_SAUDE_EST:integer NO_MACRORREGIONAL_SAUDE_EST:string CO_MESORREGIAO_EST:integer NO_MESORREGIAO_EST:string CO_MICRORREGIAO_EST:integer NO_MICRORREGIAO_EST:string CO_MICRORREGIONAL_SAUDE_EST:integer NO_MICRORREGIONAL_SAUDE_EST:string SG_UF_EST:string NO_UF_EST:string NU_ALTITUDE_MUN_EST:float NU_UF_LONGITUDE_EST:float NU_LONGITUDE_MUN_EST:float NO_REGIAO_SAUDE_EST:string
```

Code to remove generated code:
```shell
  $ rails d scaffold Dispensation
```

Code to recreate populated database
```shell
  $ rake db:recreate
```

Code to clone the extracted data
```shell script
  $ git clone git@gitlab.com:gaesi/msdaf/datasets.git modelling/extracted-datasets
```

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
