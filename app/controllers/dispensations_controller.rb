class DispensationsController < ApplicationController
  before_action :set_dispensation, only: [:show, :update, :destroy]

  # GET /dispensations
  def index
    @dispensations = Dispensation.all

    render json: @dispensations
  end

  # GET /dispensations/1
  def show
    render json: @dispensation
  end

  # POST /dispensations
  def create
    @dispensation = Dispensation.new(dispensation_params)

    if @dispensation.save
      render json: @dispensation, status: :created, location: @dispensation
    else
      render json: @dispensation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /dispensations/1
  def update
    if @dispensation.update(dispensation_params)
      render json: @dispensation
    else
      render json: @dispensation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /dispensations/1
  def destroy
    @dispensation.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dispensation
      @dispensation = Dispensation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def dispensation_params
      params.require(:dispensation).permit(:DT_DISPENSACAO, :CO_SEQ_DISPENSACAO, :QT_DISPENSACAO, :NU_COMPETENCIA_DISPENSACAO, :VL_UNITARIO, :VL_REFERENCIA_POPFARMA, :CO_SEQ_ESTABELECIMENTO, :NO_CID, :CO_CID, :ST_REGISTRO_ATIVO_CID, :ST_VIVO, :CO_PESSOA, :DT_NASCIMENTO, :DS_MES_NASCIMENTO, :CO_ANO_NASCIMENTO, :SG_SEXO_PACIENTE, :CO_MUNICIPIO_IBGE_PAC, :NO_MUNICIPIO_PAC, :CO_AGLOMERADO_URBANO_PAC, :NO_AGLOMERADO_URBANO_PAC, :CO_MACRORREGIONAL_SAUDE_PAC, :NO_MACRORREGIONAL_SAUDE_PAC, :CO_MESORREGIAO_PAC, :NO_MESORREGIAO_PAC, :CO_MICRORREGIAO_PAC, :NO_MICRORREGIAO_PAC, :CO_MICRORREGIONAL_SAUDE_PAC, :NO_MICRORREGIONAL_SAUDE_PAC, :SG_UF_PAC, :NO_UF_PAC, :NU_ALTITUDE_MUN_PAC, :NU_UF_LONGITUDE_PAC, :NU_LONGITUDE_MUN_PAC, :NO_REGIAO_SAUDE_PAC, :ST_PARTICIPA_POPFARMA_PAC, :ST_PART_FARMACIA_POPULAR_PAC, :NO_PRODUTO, :CO_PRODUTO, :NU_CATMAT, :CO_PATOLOGIA, :DS_PATOLOGIA, :DS_APRESENTACAO, :DS_UNID_APRESENTACAO, :SG_UNID_APRESENTACAO, :DS_UNID_CONCENTRACAO, :SG_UNID_CONCENTRACAO, :QT_MAXIMA, :QT_USUAL, :CO_PRINCIPIO_ATIVO_MEDICAMENTO, :NO_FABRICANTE, :NU_REGISTRO_ANVISA, :CO_GRUPO_FINANCIAMENTO, :DS_GRUPO_FINANCIAMENTO, :VL_PRECO_SUBSIDIADO, :QT_PRESCRITA, :QT_SOLICITADA, :QT_ESTORNADA, :NU_LINHA_CUIDADO, :DS_PROGRAMA_SAUDE, :SG_PROGRAMA_SAUDE, :TP_PROGRAMA_SAUDE, :ST_PARTICIPA_POPFARMA_EST, :ST_PART_FARMACIA_POPULAR_EST, :QT_POPULACA_PORTARIA_1555_2013, :CO_AGLOMERADO_URBANO_EST, :NO_AGLOMERADO_URBANO_EST, :CO_MACRORREGIONAL_SAUDE_EST, :NO_MACRORREGIONAL_SAUDE_EST, :CO_MESORREGIAO_EST, :NO_MESORREGIAO_EST, :CO_MICRORREGIAO_EST, :NO_MICRORREGIAO_EST, :CO_MICRORREGIONAL_SAUDE_EST, :NO_MICRORREGIONAL_SAUDE_EST, :SG_UF_EST, :NO_UF_EST, :NU_ALTITUDE_MUN_EST, :NU_UF_LONGITUDE_EST, :NU_LONGITUDE_MUN_EST, :NO_REGIAO_SAUDE_EST)
    end
end
