require "rails_helper"

RSpec.describe DispensationsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/dispensations").to route_to("dispensations#index")
    end

    it "routes to #show" do
      expect(:get => "/dispensations/1").to route_to("dispensations#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/dispensations").to route_to("dispensations#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/dispensations/1").to route_to("dispensations#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/dispensations/1").to route_to("dispensations#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/dispensations/1").to route_to("dispensations#destroy", :id => "1")
    end
  end
end
